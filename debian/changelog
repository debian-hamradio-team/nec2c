nec2c (1.3.1-3) unstable; urgency=medium

  * Fix edit error in the updated description
  * Clear up lintian whitespace errors

 -- Dave Hibberd <hibby@debian.org>  Tue, 19 Mar 2024 20:23:20 +0000

nec2c (1.3.1-2) unstable; urgency=medium

  * Update d/control with new description to be more clear
    * Thanks to Brian for the excellent suggestion by email

 -- Dave Hibberd <hibby@debian.org>  Sat, 24 Feb 2024 11:09:12 +0000

nec2c (1.3.1-1) unstable; urgency=medium

  * New Upstream Import
  * Add new email to d/control uploaders
  * Standards bump

 -- Dave Hibberd <hibby@debian.org>  Sat, 10 Feb 2024 18:33:59 +0000

nec2c (1.3-4) unstable; urgency=medium
  [ Debian Janitor]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 10.
  * Change priority extra to priority optional.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update Vcs-* headers from vcswatch.

  [ Dave Hibberd ]
  * Updated to DH13
    - Removed debian/compat file
  * Updated to standards 4.5.1
  * Use secure URI for uscan

 -- Dave Hibberd <d@vehibberd.com>  Wed, 27 Jan 2021 23:19:42 +0000

nec2c (1.3-3) unstable; urgency=medium

  * autogen.sh
    - Patched gnome-common macros/variables out

  * debian/patches/gnome-common-migration.patch
    - Added to patch autogen.sh
    - Closes: #830031

 -- Dave Hibberd <d@vehibberd.com>  Sat, 11 Feb 2017 20:03:08 +0000

nec2c (1.3-2) unstable; urgency=medium

  * debian/control
    - Updated to latest standards (3.9.8)
    - Changed VCS-* to secure URIs
  * debian/rules
    - Added hardening

 -- Dave Hibberd <d@vehibberd.com>  Thu, 02 Jun 2016 22:02:35 +0100

nec2c (1.3-1) unstable; urgency=medium

  * Bringing under control of Debian Hamradio Maintainers
  * Adding categories for Hamradio-Menu
  * Updated to Upstream Version 1.3
  * Added Watchfile
  * Added Manpage
  * Updated debhelper compatibility to 9

 -- Dave Hibberd <d@vehibberd.com>  Sun, 15 Feb 2015 00:27:25 +0000

nec2c (0.8-3) unstable; urgency=low

  * QA upload.
  * Fix FTBFS with --as-needed (Closes: #640719)

 -- Alessio Treglia <alessio@debian.org>  Wed, 28 Dec 2011 11:56:59 +0100

nec2c (0.8-2) unstable; urgency=low

  * Retiring - set the package maintainer to Debian QA Group.

 -- Joop Stakenborg <pa3aba@debian.org>  Mon, 02 Nov 2009 15:52:25 +0000

nec2c (0.8-1) unstable; urgency=low

  * New upstream.

 -- Joop Stakenborg <pa3aba@debian.org>  Mon, 24 Nov 2008 12:19:10 +0100

nec2c (0.6-4) unstable; urgency=low

  * Another upstream revision of the previous patch.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 20 Nov 2008 19:15:28 +0100

nec2c (0.6-3) unstable; urgency=low

  * Upstream patch to better fix the previous bug.

 -- Joop Stakenborg <pa3aba@debian.org>  Wed, 19 Nov 2008 21:22:06 +0100

nec2c (0.6-2) unstable; urgency=low

  * Fixed a segfault, thanks Jeffrey Hundstad. Closes: #498676.
    Upstream notified.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 18 Nov 2008 19:53:30 +0100

nec2c (0.6-1) unstable; urgency=low

  * Initial release.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 30 Nov 2006 17:13:03 +0100
